//
//  WidgetDialogues.swift
//  WidgetDialogues
//
//  Created by Pavel Isakov on 16.08.2022.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date())
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date())
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
}

struct WidgetDialoguesEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        VStack{
            HStack{
                Image("icon1")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                //.widgetURL(link1URL)
                
                Image("icon2")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
                Image("icon3")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
                Image("icon4")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
            }
            HStack{
                Image("icon5")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
                Image("icon6")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
                Image("icon7")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
                Image("icon8")
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .padding(10)
                // .widgetURL(link1URL)
                
            }
        }
    }
}

@main
struct WidgetDialogues: Widget {
    let kind: String = "WidgetDialogues"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            WidgetDialoguesEntryView(entry: entry)
        }
        .configurationDisplayName("Access to Dialogues")
        .description("Quick access to the last 8 dialogs")
    }
}

struct WidgetDialogues_Previews: PreviewProvider {
    static var previews: some View {
        WidgetDialoguesEntryView(entry: SimpleEntry(date: Date()))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
