//
//  MessegerApp.swift
//  Messeger
//
//  Created by Pavel Isakov on 16.08.2022.
//

import SwiftUI

@main
struct MessegerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
